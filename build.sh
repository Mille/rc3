#!/usr/bin/bash

#prepare dist folder
mkdir -p dist

#export maps
for map in $(ls *.tmx); do
	tiled --export-map --embed-tilesets "$map" "${map%.*}.json"
done
mv *.json dist/
cp -r external dist/
mkdir -p dist/tiles/imgs/tilesheets
cp -r tiles/imgs/tilesheets/* dist/tiles/imgs/tilesheets/
